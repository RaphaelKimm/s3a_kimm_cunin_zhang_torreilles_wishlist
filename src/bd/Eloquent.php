<?php

namespace mywishlist\bd;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Cette classe permet d'initialiser l'ORM Eloquent du framework Laravel.
 * Elle permet par consequent d'initialiser la connexion a la base de donnees MySQL en respectant le principe MVC.
 */
class Eloquent {

  /**
   * @var $db DB, base de donnees MySQL Eloquent
   * @var $tab array, charge les informations de connexion dans un Fichier
   */
  public static $db, $tab;

  public function setConfig($file)
  {
      if (!is_null($file)) {
            self::$tab = parse_ini_file($file);
        } else {
            throw new DBException("Error while parsing file $file\n");
        }
    }

  /**
   * Methode permettant d'initialiser la connexion a la base de donnees MySQL
   */
  public static function load()
  {
    self::$db = new DB();
    Eloquent::setConfig("src/conf/conf.ini");
    self::$db->addConnection(self::$tab);
    self::$db->setAsGlobal();
    self::$db->bootEloquent();
  }
}
