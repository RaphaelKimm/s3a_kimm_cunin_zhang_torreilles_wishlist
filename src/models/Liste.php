<?php

namespace mywishlist\models;

class Liste extends \Illuminate\Database\Eloquent\Model {
  /**
  * Attribut représentant l'objet correspondant dans la table
  */
  protected $table='liste';
  /**
  * Attribut de clé primaire indiquant son nom
  */
  protected $primaryKey='no';
  public $timestamps=false;

  /**
  * Methode permettant la definition de l'association entre item et liste
  */
  public function items(){
      return $this->hasMany('\mywishlist\models\Item', 'liste_id');
  }
}
