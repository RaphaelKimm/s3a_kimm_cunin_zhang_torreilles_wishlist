<?php

namespace mywishlist\models;

class Item extends \Illuminate\Database\Eloquent\Model {
  /**
  * Attribut représentant l'objet correspondant dans la table
  */
  protected $table='item';
  /**
  * Attributs de clé primaire indiquant son nom
  */
  protected $primaryKey='id';

  /**
  *
  */
  public $timestamps=false;

  /**
  * Methode permettant la definition de l'association entre liste et item
  */
  public function liste(){
      return $this->belongsTo('\mywishlist\models\Liste', 'liste_id');
  }
}
