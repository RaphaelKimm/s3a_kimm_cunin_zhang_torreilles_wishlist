<?php

namespace mywishlist\vue;

/**
 * La classe VueParticipant est la classe permettant de gerer l'affichage des listes par les internautes,
 * peu importe leurs droits d'acces. Ils pourront acceder aux listes et a leurs details.
 */
class VueParticipant extends VuePrincipale {

    /**
     * @var $modele Liste/Item, donnees de la base de donnees
     */
    private $modele;

    /**
     * Constructeur de VueParticipant acceptant comme parametre le modele associe a cette vue
     */
    public  function __construct($m)
    {
      $this->modele=$m;
    }

    /**
     * Methode permettant de generer un fragment de code HTML en fonction du type d'action effectue.
     * @param $selecteur int, code permettant d'identifier la methode a executer
     * @param $code array, tableau de donnees eventuel a passer lorsque des donnees supplementaires sont necessaires
     */
    public function render($selecteur, $code=NULL){
        $content = '';
        switch($selecteur){
            case 1:
                $content = $this->htmlAllList();
                break;
            case 2:
                $content = $this->htmlListAndItems();
                break;
            case 3:
                $content = $this->htmlOneItem();
                break;
            case 4:
                $content = $this->renderSearchForm();
                break;
            case 5:
                $content = $this->renderBookForm();
                break;
        }
        echo
<<<END
    $content
END;
    }

    /**
     * Methode permettant de generer le fragment HTML associe a l'affichage des listes publiques
     * @return string, liste des listes publiques
     */
    private function htmlAllList(){
        $slim=\Slim\Slim::getInstance();
        $affichageDesListes="<div class='liste'>\n
                              <h2>Listes publiques</h2>\n";
        $nb=$this->modele->count();
        if ($nb>0)
        {
          foreach($this->modele as $liste) {
            $url=$slim->urlFor('listerItemsGlobaux',['idListe' => $liste->no]);
            $affichageDesListes .= "<p>\n
                <h3><a href='$url'> $liste->no   -   $liste->titre </a></h3>\n
                <br /> Description :  . $liste->description \n
                <br /> Date d'expiration : ".date('Y-m-d',strtotime($liste->expiration))." <br /> \n
            </p>";
          }
        }
        else {
          $affichageDesListes .= "<p>
              <h2>Aucune liste publique n\'existe</h2>
              </p>";
        }
         $affichageDesListes .=  '</div>';
          return $affichageDesListes;
     }

        /**
         * Methode permettant de generer le fragment HTML associe a l'affichage d'un item
         * @param $itemListe Item, item a afficher
         * @return string, fragment HTML permettant la generation des details de l'item
         */
        private function htmlOneItem($itemListe=NULL){
          $tempModele=$this->modele;
          $itemDetails="<div class='item'>\n";
          $img='';

          if (isset($itemListe)) {
            $tempModele=$itemListe;
          }

          $slim=\Slim\Slim::getInstance();
          $url=$slim->urlFor('item', ['id' => $tempModele[0]->id, 'token' => $tempModele[0]->token]);
          $urlReserv=$slim->urlFor('itemReservation',['id' => $tempModele[0]->id, 'token' => $tempModele[0]->token]);

          $img="<img class='photo' src='".VuePrincipale::$racine."/web/img/".$tempModele[0]->img."' alt='Photo de ".$tempModele[0]->nom."'>\n";


          $itemDetails.="<img class='icone' src='".VuePrincipale::$racine."/web/img/gift.png' alt=' '>\n
                          <h3><a href='$url'>".$tempModele[0]->nom."</a></h3>\n
                          $img
                          <p>\n
                            <br/> Nom : ".$tempModele[0]->nom.
                            "<br/> URL : ".$tempModele[0]->url.
                            "<br/> Tarif : ".$tempModele[0]->tarif.
                          "</p>\n
                          </div>\n
                          <div class='reservationItem'>\n
                            <img class='icone' src='".VuePrincipale::$racine."/web/img/book.png' alt=' '>";

          if (empty($tempModele[0]->nomUtilReserv)) {
            if (empty($_SESSION['nomUtilReserv'])) {
              $value='';
            }
            else {
              $value=$_SESSION['nomUtilReserv'];
            }
            $itemDetails.="<h2>Vous voulez réserver ce cadeau ? Indiquez votre nom ici :</h2>\n
                              <form id=itemBook' method='POST' action='$urlReserv'>\n
                                <input type='text' name='nomUtilReserv' placeholder='<votre nom>' value ='$value' required>\n
                                <button type='submit'>Réserver</button>\n";
          }
          else if (isset($_SESSION['nomUtilReserv'])) {
            $itemDetails.="<h2>Votre participation a été enregistrée !</h2>\n";
            setcookie("reservationValidee",NULL,time());
          }
          else {
            $itemDetails.="<h2>Cet item est déjà réservé par ".$tempModele[0]->nomUtilReserv."</h2>\n";
          }

          return $itemDetails;

        }

        /**
         * Methode permettant de generer le fragment HTML associe a l'affichage des details d'une liste et ses items
         * @return string, fragment HTML permettant la generation des details de la liste, mais egalement ses items
         */
        private function htmlListAndItems(){

            $listeItems="<div class='listeDetails'>\n
                <h2> ".$this->modele[0]->titre."</h2>\n
                  <p> Numéro de la liste : ".$this->modele[0]->no.
                    "<br/> Date d'expiration de la liste : ".date('d/m/Y',strtotime($this->modele[0]->expiration)).
                  "</p>\n";

            $slim=\Slim\Slim::getInstance();

            if (isset($this->modele[1]))
            {
              foreach($this->modele[1] as $item){
                $url=$slim->urlFor('item', ['id' => $item->id, 'token' => $item->token]);
                $listeItems .= "<h3><a class='itemDetails' href='$url'>$item->nom</a></h3>\n";
              }
            }

            $listeItems .= '</div>';
            return $listeItems;
        }




    }
