<?php

namespace mywishlist\vue;



class VuePrincipale {

  /**
   * @var string $racine Racine du site WEB mywishlist
   */
  protected static $racine;

  /**
   * Methode permettant d'afficher soit l'accueil soit une erreur en fonction du selecteur selectionne par le controleur
   * @param int $selecteur Selecteur associant un generateur de code HTML specifique
   * @param string $code Code optionnel permettant de definir un message d'erreur
   */
  public function render($selecteur,$code=NULL)
  {
    try {
      switch ($selecteur)
      {
        case 1:
          $this->htmlAccueil();
          break;
        default:
          throw new ExceptionWL($code);
          break;
      }
    }
    catch (ExceptionWL $e)
    {
      echo
<<<END
$e;
END;
    }
  }

  /**
   * Methode permettant d'afficher la partie Accueil du site
   */
  public function htmlAccueil()
  {
    echo
<<<END
        <div class="accueil">
          <h2>Bienvenue</h2>
          <p>Vous êtes actuellement connecté sur le site qui vous permettra de réserver Noël en avance, alors n'hésitez pas !</p>
          <p>En utilisant ce site, vous serez vous pourrez ainsi utiliser les fonctionnalités suivantes :
            <ul>
              <li>Créer des listes, par le biais du bouton "Créer une liste"</li>
              <li>Accéder à une liste en particulier, par le biais du bouton "Accéder"</li>
              <li>Réserver un cadeau pour votre WishListeur, par le biais du bouton "Réserver"</li>
            </ul>
          </p>
        </div>
END;
  }

  /**
   * Methode permettant de definir les proprietes du site Web et son en-tete
   */
  public static function definitionHTML()
  {
    $slim=\Slim\Slim::getInstance();
    self::$racine=str_replace("/index.php/","",$slim->urlFor('accueil'));
    $racine=VuePrincipale::$racine;
    $accueil=$slim->urlFor('accueil');
    $toutesListes=$slim->urlFor('allLists');
    $listeSpecifique=$slim->urlFor('listItemSaisie');
    $creerListe=$slim->urlFor('creerListe');
    $modifierListe=$slim->urlFor('modifierListe');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');
    echo
<<<END
    <!DOCTYPE html>
      <html lang="fr">
        <head>
              <meta charset="UTF-8">
              <base href=$racine/>
              <link href='$racine/web/css/style.css' rel="stylesheet">
              <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
              <title> MyWishList </title>
              <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
              <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
              <script src="$racine/web/js/script.js"></script>
      </head>
        <body>
          <div class="container">

            <img src="$racine/web/img/logo.gif" alt="MyWishList"/>
              <ul class="menu">
                <li class="menu--nav"><a href="$accueil">Accueil</a></li>
                <li class="menu--nav"><a href="javascript:void(0);">Afficher une liste</a>
                <ul class="menu--sousmenu">
                    <li class="menu--devoilersousmenu"><a href="$toutesListes">Afficher les listes publiques</a></li>
                    <li class="menu--devoilersousmenu"><a href="$listeSpecifique">Afficher une liste</a></li>
                </ul>
                </li>
                <li class="menu--nav"><a href="javascript:void(0);">Administration</a>
                <ul class="menu--sousmenu">
                    <li class="menu--devoilersousmenu"><a href="$creerListe">Créer une liste</a></li>
                    <li class="menu--devoilersousmenu"><a href="$modifierListe">Modifier une liste</a></li>
                </ul>
                </li>

              </ul>
END;
  }

  /**
   * Methode permettant de definir le pied de page du site Web
   */
  public static function finHTML()
  {
    $racine=VuePrincipale::$racine;
    echo
<<<END
          </div>
          <footer>
            <img src="$racine/web/img/facebook.png" alt="Visitez notre page Facebook"/>
            <img src="$racine/web/img/twitter.png" alt="Visitez notre page Twitter"/>
            <h1>KIMM Raphaël, ZHANG Xiechen, CUNIN Maxime, TORREILLES Gabriel</h1>
          </footer>
        </body>
      </html>
END;
  }

  /**
   * Methode permettant de generer un formulaire de saisie d'identifiant d'une liste
   * @return string, formulaire de saisie de l'identifiant d'une liste
   */
  protected function renderSearchForm()
  {
    return "<div class='listeDetails'>
    <h2>Entrez l'identifiant de la liste :</h2>\n
          <form id='listSearch' method='GET'>\n
            <input type='text' name='token' placeholder='<token>'>\n
            <button type='submit'>Valider</button>\n
          </div>";
  }

}
