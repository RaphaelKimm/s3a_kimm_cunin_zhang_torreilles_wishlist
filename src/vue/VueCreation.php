<?php


namespace mywishlist\vue;

use mywishlist\models\Item;

require_once __DIR__ . "\ExceptionWL.php";

/**
 * La classe VueCreation est la classe permettant de gerer l'affichage des formulaires, listes d'items et
 * boutons (modification, ajout) qui sont lies aux operations d'ajout et de modification des listes.
 */
class VueCreation extends VuePrincipale
{

		/**
		 * @var $modele Liste/Item, donnees associees a la vue
		 */
		private $modele;

		/**
		 * Constructeur acceptant comme parametre, le modele de la vue, les donnees de la base de donnees
		 */
    public function __construct($m)
    {
    	$this->modele=$m;
    }

		/**
		 * Methode permettant de generer un fragment de code HTML en fonction du type d'action effectue.
		 * @param $selecteur int, code permettant d'identifier la methode a executer
		 * @param $code array, tableau de donnees eventuel a passer lorsque des donnees supplementaires sont necessaires
		 */
    public function render($selecteur,$code=null)
    {
        try {
            switch ($selecteur)
            {
                case 1:
                    $content = $this->htmlCreerModifierListe();
                    break;
								case 2:
		                $content = $this->htmlEditionListe();
		                break;
								case 3:
										$content = $this->htmlCreerModifierListe();
										break;
                case 4:
								case 5:
                    $content = $this->htmlAjouterModifierItem();
                    break;
                case 6:
                		 $content = $this->htmlToken();
                    break;
                case 7:
                    $content = $this->renderSearchForm();
                    break;
                default:
                    throw new ExceptionWL("Erreur.\n L'action souhaitée n'existe pas sur ce site Web");
                    break;
            }
						echo
<<<END
$content
END;
        }
        catch (ExceptionWL $e)
        {
            echo $e;
        }
    }

		/**
		 * Methode permettant de generer le code HTML associe a la creation d'une liste, ou bien a
		 * l'edition de ses informations
		 * @return string, fragment HTML associe au formulaire de creation/modification de la liste
		 */
    private function htmlCreerModifierListe(){
				$value=['', '', '', ''];

				if (isset($this->modele))
				{
					$value[0]=$this->modele->titre;
					$value[1]=$this->modele->description;
					$value[2]=$this->modele->expiration;

					if (isset($this->modele->token))
					{
						$value[3]='selected';
					}

				}

        return "<div class='listeDetails'>\n
				<h2>Entrez les informations de la liste :</h2>\n
            <form id='listAdd' method='POST'>\n

            <label> Nom de la liste :
 									   <input type='text' name='nomListe'  placeholder='<nom de la liste>' value='".$value[0]."' required>
  								</label><br/>\n

            <label> Description:
 									   <input type='text' name='descriptionListe'  placeholder='<description>' value='".$value[1]."'>
  								</label><br/>\n

            <label> Date d'expiration:
 									   <input type='text' name='expirationListe'  id='expiration' placeholder='<expiration>' value='".$value[2]."'>
  								</label><br/>\n

						<input type='checkbox' name='typeListe' value='public' ".$value[3]."'>Liste publique</input><br/>\n

            <button type='submit'>Valider</button>\n
					</div>";
    }


		/**
		 * Methode permettant de generer le code HTML associe a l'ajout d'un item, ou bien a l'edition de ses
		 * informations
		 * @return string, fragment HTML associe au formulaire de modification ou d'ajout d'un item
		 */
    private function htmlAjouterModifierItem(){
			$value=['', '', '', '', ''];


			if (isset($this->modele))
			{
				$value[0]=$this->modele->nom;
				$value[1]=$this->modele->descr;
				$value[2]=$this->modele->tarif;
				$value[3]=$this->modele->url;
				$value[4]=$this->modele->img;
			}
        return "<div class='listeDetails'>
				<h2>Entrez les informations de l'item :</h2>\n
            <form id='itemAdd' method='POST'>\n

            <br/><label> Nom de l'item :
 									   <input type='text' name='nomItem'  placeholder=".'"<nom de l\'item>"'." value='".$value[0]."' required>
  								</label>.\n

            <br/><label> Description :
 									   <input type='text' name='descriptionItem'  placeholder='<description>' value='".$value[1]."'>
  								</label>\n

            <br/><label> Prix :
 									   <input type='text' name='prixItem'  placeholder='<prix>' value='".$value[2]."'>
  								</label>\n

            <br/><label> Lien vers un site de e-commerce:
 									   <input type='text' name='lienItem'  placeholder='<lien>' value='".$value[3]."'>
  								</label>\n

            <br/><label> Image :
 									   <input type='text' name='imageItem'  placeholder='<image>' value='".$value[4]."'>
  								</label>\n

            <br/><button type='submit'>Valider</button>\n
					</div>";
    }


    /**
     * Methode retournant les informations d'une liste creee, ses tokens de modification/partage si défini
		 * @return string, fragment HTML indiquant le token de modification de la liste, le token de partage si la liste creee est publique
		 */
    private function htmlToken(){
    	$html="<div class='listeDetails'>\n
							<h2>La liste a bien été créée</h2>\n
      					<ul>\n
      					<li>Token de modification : ".$this->modele[0]."</li>\n";

      if (isset($this->modele[1]))
      {
      	$html.="<li>Token de partage : ".$this->modele[1]."</li>\n
        			</ul>\n
						</div>";
      }

      return $html;
    }


		/**
		 * Methode permettant de generer le code HTML associe a l'edition d'une liste grace a son token de modification
		 * @return string, fragment HTML associe a la page de gestion d'une liste
		 */
    private function htmlEditionListe(){
      $slim=\Slim\Slim::getInstance();
      $urlInfos=$slim->urlFor('modifierInfosListe',['idListe' => $this->modele[0]->no, 'token' => $this->modele[0]->tokenModification]);
      $urlAjout=$slim->urlFor('ajouterItem',['idListe' => $this->modele[0]->no, 'token' => $this->modele[0]->tokenModification]);
      $listeItems="<div class='listeDetails'>
            <h2>".$this->modele[0]->titre ."</h2>\n
              <p> Numéro de la liste : ".$this->modele[0]->no.
                "<br/> Date d\'expiration de la liste : ".$this->modele[0]->expiration.
                "<br/> Identifiant : ".$this->modele[0]->token.
                "<div id='boutons'>\n
								<a href='".$urlInfos."' class='bouton'>Modifier les informations</a>\n
                <a href='$urlAjout' class='bouton'>Ajouter un item</a>\n
								</div>
						  </p>
						</div>";

					if (isset($this->modele[1]))
					{
						$listeItems.="<div class='itemDetails'>
						<h2>Items de la liste</h2>\n";

						foreach($this->modele[1] as $item){
				        $url=$slim->urlFor('modifierItem', ['idItem' => $item->id, 'tokenItem' => $item->token, 'idListe' => $item->liste_id, 'tokenListe' =>$this->modele[0]->tokenModification]);
				        $listeItems .= "<h3><a href='$url'>$item->nom</a>\n
															<img src='".VuePrincipale::$racine."/web/img/delete.png' alt=''></h3>\n
														";
				     }
					}

      $listeItems .= "</div>\n
				</div>";
      return $listeItems;
    }


}
