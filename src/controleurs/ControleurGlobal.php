<?php

namespace mywishlist\controleurs;

use mywishlist\bd\Eloquent;

use mywishlist\models\Liste;

use mywishlist\models\Item;

use mywishlist\vue\VuePrincipale;

use mywishlist\vue\VueParticipant;

use mywishlist\vue\VueCreation;

/**
 * La classe ControleurGlobal permet d'afficher les éléments globaux du site (message d'accueil, mise en page), et communs à différents modules.
 *
 * @author KIMM Raphaël
 */
class ControleurGlobal {

  /**
   * Cette méthode permet d'afficher l'accueil du site
   */
  public function afficherAccueil() {
    $v = new VuePrincipale();
    $v->render(1);
  }

  /**
   * Cette méthode permet d'afficher la liste détaillée qui possède un token (identifiant) spécifique.
   *
   * @param string $token, identifiant de la liste, peut etre fourni par une entrée sur le site, ou par le navigateur
   */
  protected function afficherListe($token,$edition, $type) {
    
    if ($type=='token') {

      if ($_GET['token'])
        $listeSpe= Liste::where($type,'=',$_GET['token'])->get();
      else
        $listeSpe= Liste::where($type,'=',$token)->get();
    }
    else if ($type!='publique'){
      if ($_GET['token'])
      {
        $listeSpe= Liste::where('tokenModification','=', $_GET['token'])->get();
      }
      else
        $listeSpe= Liste::where('tokenModification','=',$token)->get();
    }
    else {
      $listeSpe= Liste::where('no','=', $token)->get();
    }


    if (count($listeSpe)>0) {
      $objets=null;

      foreach($listeSpe as $liste)
        $objets[0]=$liste;

        $itemsListeSpe=$listeSpe;

        foreach($itemsListeSpe as $liste)
        {
          for ($i=0;$i<$liste->items()->count();$i++)
          {
            $item=$liste->items()->skip($i)->first();
            $objets[1][]=$item;
          }
        }

        if ($edition) {
          $v = new VueCreation($objets);
        }
        else {
          $v = new VueParticipant($objets);
        }
        $v->render(2);

    }
    else {
      $v = new VuePrincipale();
      $v->render(null,"La liste n'existe pas");
    }
  }
}
