<?php

namespace mywishlist\Controleurs;

use mywishlist\bd\Eloquent;

use mywishlist\models\Liste;

use mywishlist\models\Item;

use mywishlist\vue\VueCreation;

use mywishlist\vue\VuePrincipale;

require_once "ControleurGlobal.php";

/**
 * La classe ControleurCreation est une classe derivee de ControleurGlobal car elle possede une methode d'affichage commune a
 * ControleurParticipation. Cette classe permet de gerer l'ensemble des operations d'administration des donnnes (creation de
 * liste, modification de liste, ajout d'item, modification d'item).
 */
class ControleurCreation extends ControleurGlobal
{
		/**
		 * Methode permettant d'afficher un formulaire/de sauvegarder les donnees saisies avec la methode correspondante, il s'agit d'un selecteur
		 *
		 * @param $action int, code identifiant l'action a effectuer
		 * @param $param tableau de donnees optionnel a passer en cas de modification, afin de recuperer les donnees dans la base de donnees
		 */
		public function afficherFormulaire($action, $param=null)
    {
			$v = new VueCreation(null);

			try {
				if ($_SERVER['REQUEST_METHOD']=='POST')
				{
					switch ($action) {
	      		case 1:
							$this->creerModifierListe(false);
							break;
						case 3:
	          	$this->creerModifierListe(true, $param[0], $param[1]);
	          	break;
	        	case 4:
	        		$this->ajouterModifierItem(false,null,null,$param[0],$param[1]);
							break;
						case 5:
							$this->ajouterModifierItem(true,$param[0],$param[1],$param[2],$param[3]);
						break;
					}
				}
				else {
					switch ($action)
					{
						case 3:
							$liste=Liste::where(['no' => $param[0], 'tokenModification' => $param[1]])->first();
							$v = new VueCreation($liste);
							$v->render($action);
							break;
						case 7:
							if (!isset($_GET['token']))
							{
								$v->render($action);
							}
							else
							{
								$this->afficherListe($_GET['token'],true,'tokenModification');
							}
							break;
						case 4:
						case 5:
							if ($action==5)
							{
								$listeTokenExistant=Liste::where('tokenModification','=',$param[3])->count();
								if ($listeTokenExistant>0)
								{
									$item= Item::where(['id' => $param[0], 'token' => $param[1]])->first();
									$v = new VueCreation($item);
								}
								else {

									$v= new VuePrincipale(null);
									$v->render(null,"La liste n'existe pas");
								}
							}
							else {

								$v = new VueCreation(null);
							}
							$v->render($action);
							break;
							default:
								$v->render($action);
								break;
				}

			}

		} catch (ExceptionWL $e)
		{
			echo $e;
		}


    }

		/**
		 * Methode permettant de creer ou de modifier une liste
		 * @param $edit boolean, indique si l'item est edite (true) ou ajoute (false)
		 * @param $idListe int, identifiant de la liste
		 * @param $tokenListe string, token de modification de la liste
		 */
    public function creerModifierListe($edit, $idListe=null, $tokenModification=null)
    {
				$nbListes=1;

				if (!$edit) {
        	$nbListes = Liste::where('titre','=',filter_var($_POST['nomListe'],FILTER_SANITIZE_STRING))->count();
				}
				else {
					$slim=\Slim\Slim::getInstance();
	        $liste=Liste::where(['no' => $idListe, 'tokenModification' => $tokenModification])->first();
				}

				$token=[];
				$bytes = random_bytes(15);
				$tokenModification = bin2hex($bytes);
				$token[]=$tokenModification;

				$bytes = random_bytes(10);
				$tokenPartage = bin2hex($bytes);
				$token[]=$tokenPartage;


        if ($nbListes == 0 && !$edit) {
            $liste = new Liste();
            $liste->titre = filter_var($_POST['nomListe'], FILTER_SANITIZE_STRING);
            $liste->description = filter_var($_POST['descriptionListe'], FILTER_SANITIZE_STRING);
            $liste->expiration = date('Y-m-d',strtotime(filter_var($_POST['expirationListe'], FILTER_SANITIZE_STRING)));

						$liste->tokenModification = $token[0];
            $liste->save();

						if (!isset($_POST['typeListe']))
						{
							$liste->token = $token[1];
	            $liste->save();
						}


          	$v=new VueCreation($token);
          	$v->render(6);

        } else if ($nbListes>0 && !$edit) {
					$v = new VuePrincipale();
					$v->render(null, "La liste existe déjà");
				}
				else if ($edit) {
					if(isset($liste)) {
							$liste->titre = filter_var($_POST['nomListe'], FILTER_SANITIZE_STRING);
							$liste->description = filter_var($_POST['descriptionListe'], FILTER_SANITIZE_STRING);
							$liste->expiration = date('Y-m-d',filter_var($_POST['expirationListe'], FILTER_SANITIZE_STRING));

							if (!isset($liste->token) && !isset($_POST['typeListe']))
							{
								$liste->token = $token[1];
		            $liste->save();
							}

							$liste->save();
							header('Location: '.$slim->urlFor('modifierListe').'?token='.$liste->tokenModification);
							exit();
						}
						else {
							$v = new VuePrincipale();
	            $v->render(null, "La liste n'existe pas");
						}
					}


    }

		/**
		 * Methode permettant d'ajouter ou de modifier un item d'une liste
		 * @param $edit boolean, indique si l'item est edite (true) ou ajoute (false)
		 * @param $idItem int, identifiant de l'item
		 * @param $tokenItem string, token d'identification de l'item
		 * @param $idListe int, identifiant de la liste
		 * @param $tokenListe string, token d'identification de la liste
		 */
    public function ajouterModifierItem($edit,$idItem=null, $tokenItem=null,$idListe=null, $tokenListe=null)
    {

        $slim=\Slim\Slim::getInstance();
				if (!$edit) {

        	$item = Item::where('nom','=',filter_var($_POST['nomItem'],FILTER_SANITIZE_STRING))->first();
					if (!isset($item)) {
						$item=new Item();
					}
				}
				else {

					$item= Item::where(['id' => $idItem, 'token' => $tokenItem])->first();
				}

        if (isset($item)) {
						$bytes = random_bytes(10);
						$token = bin2hex($bytes);
						$item->liste_id=filter_var($idListe, FILTER_SANITIZE_STRING);
            $item->nom = filter_var($_POST['nomItem'], FILTER_SANITIZE_STRING);
            $item->descr = filter_var($_POST['descriptionItem'], FILTER_SANITIZE_STRING);
            $item->tarif = filter_var($_POST['prixItem'], FILTER_SANITIZE_STRING);
            $item->url = filter_var($_POST['lienItem'], FILTER_SANITIZE_STRING);
            $item->img = filter_var($_POST['imageItem'], FILTER_SANITIZE_STRING);
						$item->token = $token;
            $item->save();
						header('Location: '.$slim->urlFor('modifierListe').'?token='.$tokenListe);
						exit();
        }

    }




}
