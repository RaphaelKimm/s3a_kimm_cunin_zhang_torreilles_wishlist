<?php

namespace mywishlist\controleurs;

use mywishlist\bd\Eloquent;

use mywishlist\models\Liste;

use mywishlist\models\Item;

use mywishlist\vue\VuePrincipale;

use mywishlist\vue\VueParticipant;
/**
 * Cette classe correspond à un controleur associé à la visualisation des listes et de leurs items.
 */
class ControleurConsultation extends ControleurGlobal{

  /**
  * Methode permettant d'afficher un item appartenant à une liste
  * @param $idea
  * @param $token
  *
  * @return item l'item à afficher
  */
  public function afficherItem($id, $token) {
    if (isset($_GET['id']) && isset($_GET['token']))
      $item= Item::where(['id' => $_GET['id'],'token' => $_GET['token']])->first();
    else
      $item=Item::where(['id' => $id,'token' => $token])->first();
    if (!is_null($item))
    {
      if (isset($_POST['nomUtilReserv']))
      {
        $item->nomUtilReserv=filter_var($_POST['nomUtilReserv'],FILTER_SANITIZE_STRING);
        $item->save();
        $_SESSION['nomUtilReserv']=$item->nomUtilReserv;
      }

      if(isset($_POST['messagePublic'])){
          $item->messagePublic= filter_var($_POST['messagePublic'], FILTER_SANITIZE_STRING);
          $item->save();
          $_SESSION['messagePublic']=$item->messagePublic;
      }

      $v= new VueParticipant([$item]);
      $v->render(3);
    }
    else {
      $v= new VuePrincipale();
      $v->render(null, "L'item n'existe pas.");
    }
  }

  /**
  * Methode affichant toutes les listes accessibles à tout le monde
  *
  * @return . toutes les listes publiques
  */
  public function afficherListesPubliques() {

    $db = Eloquent::$db;
    $liste = $db::table('liste')->whereNull('token')->get();

    $v = new VueParticipant($liste);
    $v->render(1);
  }

  /**
  * Methode permettant d'afficher le formulaire de modification d'une liste
  */
  public function afficherFormulaireListe() {

    if (!isset($_GET['token']))
    {
      $v = new VueParticipant(null);
      $v->render(4);
    }
    else
    {
      $this->afficherListe($_GET['token'],false,'token');
    }

  }

  /**
   * Methode parente de afficherListe adaptee pour fonctionner avec les listes publiques
   */
  public function afficherListeSup($idListe,$edit,$type) {
    parent::afficherListe($idListe,false,'publique');
  }
}
