<?php
/**
 * Fichier central (principal) du site.
 *
 * @author KIMM Raphaël
 */
ob_start();
// On inclut le framework Slim et de l'ORM du framework Laravel
require_once __DIR__ . '/vendor/autoload.php';

// Utilisation d'alias dans l'optique de raccourcir la longueur des namespaces
use mywishlist\bd\Eloquent;

use mywishlist\models\Liste;

use mywishlist\models\Item;

use mywishlist\controleurs\ControleurGlobal;

use mywishlist\controleurs\ControleurConsultation;

use mywishlist\controleurs\ControleurCreation;

use mywishlist\vue\VueParticipant;

use mywishlist\vue\VuePrincipale;

// On initialise la connexion a la base de donnees a l'aide de l'ORM
$db=Eloquent::load();

//  On initialise la session
session_start();

// Initialisation du framework Slim
$slimFw = new \Slim\Slim();

// Definition de routes
$slimFw->get('/listes', function (){
  $c = new ControleurConsultation();
  $c->afficherListesPubliques();
})->name('allLists');

$slimFw->get('/listes/creerListe', function (){
  $c = new ControleurCreation();
  $c->afficherFormulaire(1);
})->name('creerListe');

$slimFw->post('/listes/creerListe', function (){
  $c = new ControleurCreation();
  $c->afficherFormulaire(1);
})->name('listeCreee');

$slimFw->get('/items/liste/:idListe', function ($idListe){
  $c = new ControleurConsultation();
  $c->afficherListeSup($idListe,false,'publique');
})->name('listerItemsGlobaux');

$slimFw->get('/items/accederListe', function (){
  $c = new ControleurConsultation();
  $c->afficherFormulaireListe();
})->name('listItemSaisie');


$slimFw->get('/items/modifierListe', function (){
  $c = new ControleurCreation();
  $c->afficherFormulaire(7);
})->name('modifierListe');

$slimFw->get('/items/ajouterItem/:idListe/:token', function ($idListe,$token){
  $c = new ControleurCreation();
  $c->afficherFormulaire(4,[$idListe,$token]);
})->name('ajouterItem');

$slimFw->post('/items/ajouterItem/:idListe/:token', function ($idListe,$token){
  $c = new ControleurCreation();
  $c->afficherFormulaire(4,[$idListe,$token]);
});

$slimFw->get('/items/modifierItem/:idItem/:tokenItem/:idListe/:tokenListe', function ($idItem,$tokenItem,$idListe,$tokenListe){
  $c = new ControleurCreation();
  $c->afficherFormulaire(5,[$idItem,$tokenItem,$idListe,$tokenListe]);
})->name('modifierItem');

$slimFw->post('/items/modifierItem/:idItem/:tokenItem/:idListe/:tokenListe', function ($idItem,$tokenItem,$idListe,$tokenListe){
  $c = new ControleurCreation();
  $c->afficherFormulaire(5,[$idItem,$tokenItem,$idListe,$tokenListe]);
});

$slimFw->get('/items/modifierListe/:idListe/:token', function ($idListe,$token){
  $c = new ControleurCreation();
  $c->afficherFormulaire(3,[$idListe,$token]);
})->name('modifierInfosListe');

$slimFw->post('/items/modifierListe/:idListe/:token', function ($idListe,$token){
  $c = new ControleurCreation();
  $c->afficherFormulaire(3,[$idListe,$token]);
});



$slimFw->get('/items/:id/:token', function ($id, $token){
  $c = new ControleurConsultation();
  $c->afficherItem($id,$token);
})->name('item');

$slimFw->post('/items/reserver/:id/:token', function ($id, $token){
  $c = new ControleurConsultation();
  $c->afficherItem($id,$token);
})->name('itemReservation');

$slimFw->get('/', function (){
  $c = new ControleurGlobal();
  $c->afficherAccueil();
})->name('accueil');

// Permet de definir l'en-tête du site
VuePrincipale::definitionHTML();




/*
Il s'agissait des premiers tests, nous les avons donc désactivé
// pour afficher la liste des listes de souhaits
 $listl = Liste::all() ;
 $vue = new VueParticipant( $listl->toArray() ) ;
 $vue->render( 1 ) ;

 // pour afficher une liste et ses items
 $listeSpe= Liste::where('no','=',2)->get();
 foreach($listeSpe as $liste)
  $objets[0]=$liste;
 $itemsListeSpe=$listeSpe;

 foreach($itemsListeSpe as $liste)
 {
   for ($i=0;$i<$liste->items()->count();$i++)
   {
     $item=$liste->items()->skip($i)->first();
     $objets[1][]=$item;
   }
 }

 $vue = new \mywishlist\vue\VueParticipant($objets) ;
 $vue->render( 2 ) ;

 // pour afficher 1 item
 $item = Item::find( 2 ) ;
 $vue = new \mywishlist\vue\VueParticipant( [ $item ] ) ;
 $vue->render( 3 ) ;*/

$slimFw->run();

// Permet de definir le pied de page du site
VuePrincipale::finHTML();
